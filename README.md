# OpenML dataset: post-operative

https://www.openml.org/d/43040

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Sharon Summers
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Post-Operative+Patient) - 1993
**Please cite**: [UCI](https://archive.ics.uci.edu/ml/citation_policy.html)  

**Post-Operative Patient Data Set**

The classification task of this database is to determine where patients in a postoperative recovery area should be sent to next. Because hypothermia is a significant concern after surgery (Woolery, L. et. al. 1991), the attributes correspond roughly to body temperature measurements.

### Attribute information

1. L-CORE (patient's internal temperature in C): 
high (> 37), mid (>= 36 and <= 37), low (< 36) 
2. L-SURF (patient's surface temperature in C): 
high (> 36.5), mid (>= 36.5 and <= 35), low (< 35) 
3. L-O2 (oxygen saturation in %): 
excellent (>= 98), good (>= 90 and < 98), 
fair (>= 80 and < 90), poor (< 80) 
4. L-BP (last measurement of blood pressure): 
high (> 130/90), mid (<= 130/90 and >= 90/70), low (< 90/70) 
5. SURF-STBL (stability of patient's surface temperature): 
stable, mod-stable, unstable 
6. CORE-STBL (stability of patient's core temperature) 
stable, mod-stable, unstable 
7. BP-STBL (stability of patient's blood pressure) 
stable, mod-stable, unstable 
8. COMFORT (patient's perceived comfort at discharge, measured as 
an integer between 0 and 20) 
9. decision ADM-DECS (discharge decision): 
I (patient sent to Intensive Care Unit), 
S (patient prepared to go home), 
A (patient sent to general hospital floor)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43040) of an [OpenML dataset](https://www.openml.org/d/43040). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43040/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43040/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43040/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

